package softagi.mansour.bookapi.network.remote;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import softagi.mansour.bookapi.models.bookModel;

public class retrofitClient
{
    private static retrofitClient retrofitClient;
    private static retrofitHelper retrofitHelper;

    private retrofitClient()
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://www.googleapis.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        retrofitHelper = retrofit.create(retrofitHelper.class);
    }

    public static retrofitClient getInstance()
    {
        if (retrofitClient == null)
        {
            retrofitClient = new retrofitClient();
        }

        return retrofitClient;
    }

    public static Call<bookModel> getBooks(String k)
    {
        return retrofitHelper.getBooks(k);
    }
}