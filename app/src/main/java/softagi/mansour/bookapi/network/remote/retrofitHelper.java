package softagi.mansour.bookapi.network.remote;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import softagi.mansour.bookapi.models.bookModel;

public interface retrofitHelper
{
    @GET("books/v1/volumes")
    Call<bookModel> getBooks(
            @Query("q") String searchKey
    );
}