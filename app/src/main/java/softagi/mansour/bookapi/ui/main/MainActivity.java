package softagi.mansour.bookapi.ui.main;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import softagi.mansour.bookapi.R;
import softagi.mansour.bookapi.models.bookModel;
import softagi.mansour.bookapi.models.bookModel.*;
import softagi.mansour.bookapi.network.remote.retrofitClient;

public class MainActivity extends AppCompatActivity
{
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initRecycler();
        checkConnection();
        refresh();
    }

    private void refresh()
    {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
        {
            @Override
            public void onRefresh()
            {
                swipeRefreshLayout.setRefreshing(true);
                checkConnection();
            }
        });
    }

    private void checkConnection()
    {
        ConnectivityManager cm =
                (ConnectivityManager)getApplication().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        if (isConnected)
        {
            getData();
        } else
            {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), "please check your internet connection", Toast.LENGTH_SHORT).show();
                swipeRefreshLayout.setRefreshing(false);
            }
    }

    private void getData()
    {
        Call<bookModel> call = retrofitClient.getInstance().getBooks("cars");
        call.enqueue(new Callback<bookModel>()
        {
            @Override
            public void onResponse(Call<bookModel> call, Response<bookModel> response)
            {
                if (response.isSuccessful())
                {
                    progressBar.setVisibility(View.GONE);
                    swipeRefreshLayout.setRefreshing(false);
                    recyclerView.setAdapter(new booksAdapter(response.body().getItems()));
                } else
                    {

                    }
            }

            @Override
            public void onFailure(Call<bookModel> call, Throwable t)
            {

            }
        });
    }

    private void initRecycler()
    {
        recyclerView = findViewById(R.id.book_recycler);
        progressBar = findViewById(R.id.progress_circular);
        swipeRefreshLayout = findViewById(R.id.books_refresh);
        recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL));
    }

    private class booksAdapter extends RecyclerView.Adapter<booksAdapter.VH>
    {
        List<listData> bookModels;

        public booksAdapter(List<listData> bookModels) {
            this.bookModels = bookModels;
        }

        @NonNull
        @Override
        public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
        {
            View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.book_item, null);
            return new VH(view);
        }

        @Override
        public void onBindViewHolder(@NonNull VH holder, int position)
        {
            listData model = bookModels.get(position);

            String title = model.getVolumeInfo().getTitle();
            String date = model.getVolumeInfo().getPublishedDate();
            //String desc = model.getVolumeInfo().getDescription();
            String author;
            if (model.getVolumeInfo().getAuthors() != null)
            {
                author = model.getVolumeInfo().getAuthors().get(0);
            } else
                {
                    author = "no author found";
                }
            String image = model.getVolumeInfo().getImageLinks().getThumbnail();

            holder.bookTitle.setText(title);
            holder.bookDate.setText(date);
            holder.bookAuthor.setText(author);

            if (image != null)
            {
                Picasso.get()
                        .load(image + ".jpg")
                        .placeholder(R.drawable.ic_book_black_24dp)
                        .error(R.drawable.ic_book_black_24dp)
                        .into(holder.bookImage);
            }
        }

        @Override
        public int getItemCount()
        {
            return bookModels.size();
        }

        private class VH extends RecyclerView.ViewHolder
        {
            ImageView bookImage;
            TextView bookTitle,bookAuthor,bookDate;

            VH(@NonNull View itemView)
            {
                super(itemView);

                bookImage = itemView.findViewById(R.id.book_image);
                bookTitle = itemView.findViewById(R.id.book_title);
                bookAuthor = itemView.findViewById(R.id.book_author);
                bookDate = itemView.findViewById(R.id.book_date);
            }
        }
    }
}